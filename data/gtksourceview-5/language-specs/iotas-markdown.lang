<?xml version="1.0" encoding="UTF-8"?>
<!--

 Author: Jean-Philippe Fleury
 Copyright (C) 2011 Jean-Philippe Fleury <contact@jpfleury.net>

 GtkSourceView is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 GtkSourceView is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with this library; if not, see <http://www.gnu.org/licenses/>.

-->
<!-- Note: this language definition file adds support for Markdown syntax,
     described in the following websites:
     * (fr) <http://michelf.com/projets/php-markdown/syntaxe/>
     * (en) <http://daringfireball.net/projects/markdown/syntax> -->

<!-- Iotas notes: this is 99% the original markdown language spec from GtkSourceView
     (the good parts) and 1% the changes for Iotas (the dubious parts).
     Changes include (but likely not limited to):
     * The ability to style the markup elements
     * Defining individual styles for each level of ATX heading
     * Checkbox support
     * Prioritising deeper indentation of lists over setext header underlining
     * Not inheriting from HTML
     * Contexts excluded: atx-header, setext-header, line-break
     * Adding strikethrough
     * Modified underscore emphasis closer to GFM where word characters can't sit
       immediately beyond the delimiters
  -->
<language id="iotas-markdown" _name="Iotas Markdown" version="2.0" _section="Markup">
  <metadata>
    <property name="mimetypes">text/x-markdown</property>
    <property name="globs">*.markdown;*.md;*.mkd</property>
    <property name="block-comment-start">&lt;!--</property>
    <property name="block-comment-end">--&gt;</property>
    <property name="suggested-suffix">.md</property>
  </metadata>

  <styles>
    <style id="header" name="Header"/>
    <style id="horizontal-rule" name="Horizontal Rule"/>
    <style id="list-marker" name="List Marker"/>
    <style id="code-span" name="Code Span"/>
    <style id="code-block" name="Code Block"/>
    <style id="blockquote-marker" name="Blockquote Marker"/>
    <style id="url" name="URL"/>
    <style id="link-text" name="Link Text"/>
    <style id="label" name="Label"/>
    <style id="attribute-value" name="Attribute Value"/>
    <style id="image-marker" name="Image Marker"/>
    <style id="emphasis" name="Emphasis"/>
    <style id="strong-emphasis" name="Strong Emphasis"/>
    <style id="strikethrough" name="Strikethrough"/>
    <style id="backslash-escape" name="Backslash Escape" map-to="def:special-char"/>
    <style id="heading1" name="Heading 1"/>
    <style id="heading2" name="Heading 2"/>
    <style id="heading3" name="Heading 3"/>
    <style id="heading4" name="Heading 4"/>
    <style id="heading5" name="Heading 5"/>
    <style id="heading6" name="Heading 6"/>
    <style id="codeblock-markup" name="Codeblock Markup"/>
    <style id="emphasis-markup" name="Emphasis Markup"/>
    <style id="strong-emphasis-markup" name="Strong Emphasis Markup"/>
    <style id="strikethrough-markup" name="Strikethrough Markup"/>
    <style id="link-markup" name="Link Markup"/>
    <style id="blockquote" name="Blockquote"/>
    <style id="heading-markup" name="Markdown Heading Markup"/>
    <style id="checkbox-markup" name="Markdown Checkbox Markup"/>
    <style id="checkbox-check" name="Markdown Checkbox Check"/>
  </styles>

  <definitions>
    <!-- Examples:
         - - -
         **  **  **  **  **
         _____
    -->
    <!-- Iotas currently prioritises horizontal rules over setext header underlining -->
    <context id="horizontal-rule" style-ref="horizontal-rule" class="horizontal-rule">
      <match extended="true">
        ^[ ]{0,3}            # Maximum 3 spaces at the beginning of the line.
        (
          (-[ ]{0,2}){3,} | # 3 or more hyphens, with 2 spaces maximum between each hyphen.
          (_[ ]{0,2}){3,} | # Idem, but with underscores.
          (\*[ ]{0,2}){3,}  # Idem, but with asterisks.
        )
        [ \t]*$              # Optional trailing spaces or tabs.
      </match>
    </context>

    <!-- Note about following list and code block contexts: according to the
         Markdown syntax, to write several paragraphs in a list item, we have
         to indent each paragraph. Example:

         - Item A (paragraph 1).

             Item A (paragraph 2).

             Item A (paragraph 3).

         - Item B.

         So there is a conflict in terms of syntax highlighting between an
         indented paragraph inside a list item (4 spaces or 1 tab) and an
         indented line of code outside a list (also 4 spaces or 1 tab). In this
         language file, since a full context analysis can't be done (because
         line break can't be used in regex), the choice was made ​​to highlight
         code block only from 2 levels of indentation. -->

    <!-- Example (unordered list):
         * Item
         + Item
         - Item

         Example (ordered list):
         1. Item
         2. Item
         3. Item
    -->
    <!-- Unlike GtkSourceView's priorities, in the Iotas markdown language file we prioritise
         more heavily indented lists over indented lines of code, plus ensure it's not a
         checkbox.  -->
    <context id="list" style-ref="list-marker" class="list-marker">
      <match extended="true">
        ^[ ]{0,24} # Maximum 3 spaces at the beginning of the line.
        (
          \*|\+|-| # Asterisk, plus or hyphen for unordered list.
          [0-9]+\. # Number followed by period for ordered list.
        )
        [ \t]+     # Must be followed by at least 1 space or 1 tab.
        (?!\[[ x]{1}\])  # Not a checkbox
      </match>
    </context>

    <!-- Example:
                 <em>HTML code</em> displayed <strong>literally</strong>.
    -->
    <context id="indented-code-block" class="no-spell-check indented-code-block">
      <match>^( {8,}|\t{2,})([^ \t]+.*)</match>

      <include>
        <context sub-pattern="2" style-ref="code-block"/>
      </include>
    </context>

    <!-- Note about following code span contexts: within a paragraph, text
         wrapped with backticks indicates a code span. Markdown allows to use
         one or more backticks to wrap text, provided that the number is identical
         on both sides, and the same number of consecutive backticks is not
         present within the text. The current language file supports code span
         highlighting with up to 2 backticks surrounding text. -->

    <!-- Examples:
         Here's a literal HTML tag: `<p>`.
         `Here's a code span containing ``backticks``.`
    -->
    <context id="1-backtick-code-span" class="no-spell-check code-span" style-ref="code-span">
      <match>(?&lt;!`)(`)[^`]+(`{2,}[^`]+)*(`)(?!`)</match>
      <include>
        <context sub-pattern="1" style-ref="codeblock-markup"/>
        <context sub-pattern="3" style-ref="codeblock-markup"/>
      </include>
    </context>

    <!-- Examples:
         Here's a literal HTML tag: ``<p>``.
         ``The grave accent (`) is used in Markdown to indicate a code span.``
         ``Here's another code span containing ```backticks```.``
    -->
    <context id="2-backticks-code-span" class="no-spell-check 2-backticks-code-span code-span" style-ref="code-span">
      <match>(?&lt;!`)(``)[^`]+((`|`{3,})[^`]+)*(``)(?!`)</match>
      <include>
        <context sub-pattern="1" style-ref="codeblock-markup"/>
        <context sub-pattern="4" style-ref="codeblock-markup"/>
      </include>
    </context>

    <context id="3-backticks-code-span" class="no-spell-check code-block" style-ref="code-block">
      <start>^(```).*$</start>
      <end>^(```)$</end>
      <include>
        <context sub-pattern="1" where="start" style-ref="codeblock-markup"/>
        <context sub-pattern="1" where="end" style-ref="codeblock-markup"/>
      </include>
    </context>

    <!-- Example:
         > Quoted text.
         > Quoted text with `code span`.
         >> Blockquote **nested**.
    -->
    <!-- Note: blockquote can contain block-level and inline Markdown elements,
         but the current language file only highlights inline ones (emphasis,
         link, etc.). -->
    <context id="blockquote" style-ref="blockquote" end-at-line-end="true" class="blockquote">
      <start>^( {0,3}&gt;(?=.)( {0,4}&gt;)*)</start>

      <include>
        <context sub-pattern="1" where="start" style-ref="blockquote-marker"/>
        <context ref="1-backtick-code-span"/>
        <context ref="2-backticks-code-span"/>
        <context ref="3-backticks-code-span"/>
        <context ref="automatic-link"/>
        <context ref="inline-link"/>
        <context ref="reference-link"/>
        <context ref="inline-image"/>
        <context ref="reference-image"/>
        <context ref="underscores-emphasis"/>
        <context ref="asterisks-emphasis"/>
        <context ref="underscores-strong-emphasis"/>
        <context ref="asterisks-strong-emphasis"/>
        <context ref="strikethrough"/>
        <context ref="backslash-escape"/>
      </include>
    </context>

    <!-- Examples:
         <user@example.com>
         <http://www.example.com/>
    -->
    <!-- Note: regular expressions are based from function `_DoAutoLinks` from
         Markdown.pl (see <http://daringfireball.net/projects/markdown/>). -->
    <context id="automatic-link" class="no-spell-check automatic-link">
      <match case-sensitive="false" extended="true">
        (&lt;)
          (((mailto:)?[a-z0-9.-]+\@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+) | # E-mail.
          ((https?|ftp):[^'">\s]+))                                     # URL.
        (&gt;)
      </match>

      <include>
        <context sub-pattern="1" style-ref="link-markup"/>
        <context sub-pattern="2" style-ref="url"/>
        <context sub-pattern="8" style-ref="link-markup"/>
      </include>
    </context>

    <!-- Examples:
         [link text](http://www.example.com/)
         [link text](<http://www.example.com/>)
         [link text]( /folder/page.html "Title" )
    -->
    <context id="inline-link" class="inline-link">
      <match extended="true">
        (\[)(.*?)(\])      # Link text.
        (\()               # Literal opening parenthesis.
          [ \t]*           # Optional spaces or tabs after the opening parenthesis.
          (&lt;(.*?)&gt; | # URL with brackets.
          (.*?))           # URL without brackets.
          ([ \t]+(".*?"))? # Optional title.
          [ \t]*           # Optional spaces or tabs before the closing parenthesis.
        (\))               # Literal closing parenthesis.
      </match>

      <include>
        <context sub-pattern="1" style-ref="link-markup"/>
        <context sub-pattern="2" style-ref="link-text"/>
        <context sub-pattern="3" style-ref="link-markup"/>
        <context sub-pattern="4" style-ref="link-markup"/>
        <context sub-pattern="6" class="no-spell-check" style-ref="url"/>
        <context sub-pattern="7" class="no-spell-check" style-ref="url"/>
        <context sub-pattern="9" style-ref="attribute-value"/>
        <context sub-pattern="10" style-ref="link-markup"/>
      </include>
    </context>

    <!-- Examples:
         [link text]
         [link text][]
         [link text][link label]
         [link text] [link label]
    -->
    <!-- Note: some assertions are used to differentiate reference link from
         link label. -->
    <context id="reference-link">
      <match>(?&lt;!^ |^  |^   )(\[)(.*?)(\])([ \t]?(\[)(.*?)(\]))?(?!:)</match>

      <include>
        <context sub-pattern="1" style-ref="link-markup"/>
        <context sub-pattern="2" style-ref="link-text"/>
        <context sub-pattern="3" style-ref="link-markup"/>
        <context sub-pattern="5" style-ref="link-markup"/>
        <context sub-pattern="6" class="no-spell-check" style-ref="label"/>
        <context sub-pattern="7" style-ref="link-markup"/>
      </include>
    </context>

    <!-- Examples:
         [link label]: /folder/page.html
         [link label]: <http://www.example.com/>
         [link label]: http://www.example.com/ "Title"
    -->
    <context id="link-definition">
      <match extended="true">
        ^[ ]{0,3}             # Maximum 3 spaces at the beginning of the line.
        (\[)(.+?)(\]:)        # Link label and colon.
        [ \t]*                # Optional spaces or tabs.
        ((&lt;)([^ \t]+?)(&gt;) | # URL with brackets.
        ([^ \t]+?))           # URL without brackets.
        ([ \t]+(")(.*?)("))?      # Optional title.
        [ \t]*$               # Optional trailing spaces or tabs.
      </match>

      <include>
        <context sub-pattern="1" style-ref="link-markup"/>
        <context sub-pattern="2" class="no-spell-check" style-ref="label"/>
        <context sub-pattern="3" style-ref="link-markup"/>
        <context sub-pattern="5" style-ref="link-markup"/>
        <context sub-pattern="6" class="no-spell-check" style-ref="url"/>
        <context sub-pattern="7" style-ref="link-markup"/>
        <context sub-pattern="8" class="no-spell-check" style-ref="url"/>
        <context sub-pattern="10" style-ref="link-markup"/>
        <context sub-pattern="11" style-ref="attribute-value"/>
        <context sub-pattern="12" style-ref="link-markup"/>
      </include>
    </context>

    <!-- Examples:
         ![alt text](http://www.example.com/image.jpg)
         ![alt text]( <http://www.example.com/image.jpg> )
         ![alt text] (/path/to/image.jpg "Title")
    -->
    <context id="inline-image">
      <match extended="true">
        (!)                     # Leading ! sign.
        (\[)(.*?)(\])[ ]?       # Alternate text for the image (and optional space).
        (\()                    # Literal parenthesis.
          [ \t]*                # Optional spaces or tabs after the opening parenthesis.
          (&lt;([^ \t]*?)&gt; | # Image path or URL with brackets.
          ([^ \t]*?))           # Image path or URL without brackets.
          ([ \t]+(".*?"))?      # Optional title.
          [ \t]*                # Optional spaces or tabs before the closing parenthesis.
        (\))                    # Literal parenthesis.
      </match>

      <include>
        <context sub-pattern="1" style-ref="image-marker"/>
        <context sub-pattern="2" style-ref="image-marker"/>
        <context sub-pattern="3" style-ref="attribute-value"/>
        <context sub-pattern="4" style-ref="image-marker"/>
        <context sub-pattern="5" style-ref="image-marker"/>
        <context sub-pattern="7" class="no-spell-check" style-ref="url"/>
        <context sub-pattern="8" class="no-spell-check" style-ref="url"/>
        <context sub-pattern="9" style-ref="attribute-value"/>
        <context sub-pattern="11" style-ref="image-marker"/>
      </include>
    </context>

    <!-- Examples:
         ![alt text][image label]
         ![alt text] [image label]
    -->
    <context id="reference-image">
      <match>(!)(\[)(.*?)(\]) ?(\[)(.*?)(\])</match>

      <include>
        <context sub-pattern="1" style-ref="image-marker"/>
        <context sub-pattern="2" style-ref="image-marker"/>
        <context sub-pattern="3" style-ref="attribute-value"/>
        <context sub-pattern="4" style-ref="image-marker"/>
        <context sub-pattern="5" style-ref="image-marker"/>
        <context sub-pattern="6" class="no-spell-check" style-ref="label"/>
        <context sub-pattern="7" style-ref="image-marker"/>
      </include>
    </context>

    <!-- Examples:
         Lorem _ipsum dolor_ sit amet.
         Here's an _emphasized text containing an underscore (\_)_.
    -->
    <context id="underscores-emphasis" style-ref="emphasis" class="italic">
      <match>(?&lt;!_)(?&lt;!\w)(_)[^_ \t].*?(?&lt;!\\|_| |\t)(_)(?!_)(?!\w)</match>
      <include>
        <context sub-pattern="1" style-ref="emphasis-markup"/>
        <context sub-pattern="2" style-ref="emphasis-markup"/>
     </include>
    </context>

    <!-- Examples:
         Lorem *ipsum dolor* sit amet.
         Here's an *emphasized text containing an asterisk (\*)*.
    -->
    <context id="asterisks-emphasis" style-ref="emphasis" class="italic">
      <match>(?&lt;!\*)(\*)[^\* \t].*?(?&lt;!\\|\*| |\t)(\*)(?!\*)</match>
      <include>
        <context sub-pattern="1" style-ref="emphasis-markup"/>
        <context sub-pattern="2" style-ref="emphasis-markup"/>
      </include>
    </context>

    <!-- Examples:
         Lorem __ipsum dolor__ sit amet.
         Here's a __strongly emphasized text containing an underscore (\_)__.
    -->
    <context id="underscores-strong-emphasis" style-ref="strong-emphasis" class="bold">
      <match>(__)[^_ \t].*?(?&lt;!\\|_| |\t)(__)</match>
      <include>
        <context sub-pattern="1" style-ref="strong-emphasis-markup"/>
        <context sub-pattern="2" style-ref="strong-emphasis-markup"/>
      </include>
    </context>

    <!-- Examples:
         Lorem **ipsum dolor** sit amet.
         Here's a **strongly emphasized text containing an asterisk (\*).**
    -->
    <context id="asterisks-strong-emphasis" style-ref="strong-emphasis" class="bold">
      <match>(\*\*)[^\* \t].*?(?&lt;!\\|\*| |\t)(\*\*)</match>
      <include>
        <context sub-pattern="1" style-ref="strong-emphasis-markup"/>
        <context sub-pattern="2" style-ref="strong-emphasis-markup"/>
      </include>
    </context>

    <!-- Examples:
         Lorem ~~ipsum dolor~~ sit amet.
    -->
    <context id="strikethrough" style-ref="strikethrough" class="strikethrough">
      <match>(\~\~)[^\* \t].*?(?&lt;!\\|\*| |\t)(\~\~)</match>
      <include>
        <context sub-pattern="1" style-ref="strikethrough-markup"/>
        <context sub-pattern="2" style-ref="strikethrough-markup"/>
      </include>
    </context>

    <context id="backslash-escape" style-ref="backslash-escape">
      <match>\\[\\`*_{}\[\]()#+-.!]</match>
    </context>

    <context id="heading1" style-ref="heading1">
      <match>^(#[ ])[^#].*</match>
      <include>
        <context sub-pattern="1" style-ref="heading-markup"/>
      </include>
    </context>

    <context id="heading2" style-ref="heading2">
      <match>^(##[ ])[^#].*</match>
      <include>
        <context sub-pattern="1" style-ref="heading-markup"/>
      </include>
    </context>

    <context id="heading3" style-ref="heading3">
      <match>^(#{3}[ ])[^#].*</match>
      <include>
        <context sub-pattern="1" style-ref="heading-markup"/>
      </include>
    </context>

    <context id="heading4" style-ref="heading4">
      <match>^(#{4}[ ])[^#].*</match>
      <include>
        <context sub-pattern="1" style-ref="heading-markup"/>
      </include>
    </context>

    <context id="heading5" style-ref="heading5">
      <match>^(#{5}[ ])[^#].*</match>
      <include>
        <context sub-pattern="1" style-ref="heading-markup"/>
      </include>
    </context>

    <context id="heading6" style-ref="heading6">
      <match>^(#{6}[ ])[^#].*</match>
      <include>
        <context sub-pattern="1" style-ref="heading-markup"/>
      </include>
    </context>

    <context id="checkbox" style-ref="checkbox-markup" class="checkbox">
      <match extended="true">
        ^[ ]{0,24}     # Maximum 24 spaces at the beginning of the line.
        (-|\*|\+)
        [ ]
        \[([ x])\]
        [ ]            # Must be followed by at least 1 space
      </match>

      <include>
        <context sub-pattern="2" style-ref="checkbox-check" class="checkbox-check"/>
      </include>
    </context>

    <context id="iotas-markdown">
      <include>
        <context ref="horizontal-rule"/>
        <context ref="list"/>
        <context ref="indented-code-block"/>
        <context ref="1-backtick-code-span"/>
        <context ref="2-backticks-code-span"/>
        <context ref="3-backticks-code-span"/>
        <context ref="blockquote"/>
        <context ref="automatic-link"/>
        <context ref="inline-link"/>
        <context ref="reference-link"/>
        <context ref="link-definition"/>
        <context ref="inline-image"/>
        <context ref="reference-image"/>
        <context ref="underscores-emphasis"/>
        <context ref="asterisks-emphasis"/>
        <context ref="underscores-strong-emphasis"/>
        <context ref="asterisks-strong-emphasis"/>
        <context ref="strikethrough"/>
        <context ref="backslash-escape"/>
        <context ref="heading1"/>
        <context ref="heading2"/>
        <context ref="heading3"/>
        <context ref="heading4"/>
        <context ref="heading5"/>
        <context ref="heading6"/>
        <context ref="checkbox"/>
      </include>
    </context>
  </definitions>
</language>
